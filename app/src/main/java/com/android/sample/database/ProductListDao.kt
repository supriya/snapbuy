/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.sample.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.android.sample.model.Categories

/**
 * Database Access Object for the category list  database.
 */
/**
 * Created by Supriya on 15/04/20.
 */
@Dao
interface ProductListDao {
    /**
     * We can use LiveData also here if required , to serve data in the UI viva viewmodel,
     * Method to get all the category list data
     */
    @Query("SELECT * FROM category_list_table")
    suspend fun getAllData(): List<Categories>

    @Insert
    suspend fun insert(categoriesList: List<Categories>)

    @Query("DELETE FROM category_list_table")
    suspend fun deleteAll()

}