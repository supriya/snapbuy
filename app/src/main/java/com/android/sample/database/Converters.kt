package com.android.sample.database

import androidx.room.TypeConverter
import com.android.sample.model.CategoryItems
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type
import java.util.*

/**
 * Created by Supriya on 15/04/20.
 */
class Converters {
    var gson = Gson()

    @TypeConverter
    fun stringToSomeObjectList(data: String?): List<CategoryItems?>? {
        if (data == null) {
            return Collections.emptyList()
        }
        val listType: Type =
            object : TypeToken<List<CategoryItems?>?>() {}.type
        return gson.fromJson<List<CategoryItems?>>(data, listType)
    }

    @TypeConverter
    fun someObjectListToString(someObjects: List<CategoryItems?>?): String? {
        return gson.toJson(someObjects)
    }
}