package com.android.sample.database

import androidx.room.*
import android.content.Context
import com.android.sample.model.Categories
import com.android.sample.utils.AppConstants

/**
 * Singleton database object. Note that for a real app, you should probably use a Dependency
 * Injection framework or Service Locator to create the singleton database.
 */
/**
 * Created by Supriya on 15/04/20.
 */
@Database(entities = [Categories::class], version = 1)
@TypeConverters(Converters::class)
abstract class ProductListDataBase : RoomDatabase() {
    abstract fun productListDao(): ProductListDao

    companion object {
        private var instance: ProductListDataBase? = null

        @Synchronized
        fun get(context: Context): ProductListDataBase {
            if (instance == null) {
                instance = Room.databaseBuilder(
                        context.applicationContext,
                        ProductListDataBase::class.java, AppConstants.SHOP_DATA_BASE
                    ).build()
            }
            return instance!!
        }
    }
}