package com.android.sample.module.product.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.FragmentTransaction
import com.android.sample.R
import com.android.sample.module.IShopCommunicator
/**
 * Created by Supriya on 15/04/20.
 */
class ProductListActivity : AppCompatActivity(),IShopCommunicator {
    private val SHOP_CART_FRAGMENT_TAG = "shop_cart_fragment_tag"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.product_list_activity_view)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container,
                    ProductListFragment.newInstance()
                )
                .commitNow()
        }
    }

    override fun launchCartsFragment() {

        val transaction: FragmentTransaction =
            supportFragmentManager.beginTransaction()
        transaction.add(R.id.container,
            CartFragment.newInstance(), SHOP_CART_FRAGMENT_TAG)
        transaction.addToBackStack(null)
        transaction.commit()
    }
}
