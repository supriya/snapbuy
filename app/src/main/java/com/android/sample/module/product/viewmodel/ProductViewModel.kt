package com.android.sample.module.product.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.android.sample.database.ProductListDao
import com.android.sample.database.ProductListDataBase
import com.android.sample.model.BannerListItem
import com.android.sample.model.Categories
import com.android.sample.model.CategoryItems
import com.android.sample.network.NetWorkApi
import com.android.sample.utils.GenericUtils
import com.android.sample.utils.Logger
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.launch
import java.lang.Exception

/**
 * Created by Supriya on 15/04/20.
 */
class ProductViewModel(application: Application) : AndroidViewModel(application) {


    private val dao: ProductListDao =
        ProductListDataBase.get(application).productListDao() //intialize the data base

    private var _bannerDataList: MutableLiveData<List<BannerListItem>> = MutableLiveData()

    //data exposed to view
    val bannerDataList: LiveData<List<BannerListItem>>
        get() = _bannerDataList


    private var _productList: MutableLiveData<List<Categories>> = MutableLiveData()

    //data exposed to view
    val productList: LiveData<List<Categories>>
        get() = _productList

    private var _cartList: MutableLiveData<ArrayList<CategoryItems>> = MutableLiveData()

    //data exposed to view
    val cartList: LiveData<ArrayList<CategoryItems>>
        get() = _cartList

    var cartTotal = MutableLiveData<String>()
    var showProgressBar = MutableLiveData<Boolean>(false)

    private var disposable: Disposable? = null

    private val netWorkApi by lazy {
        NetWorkApi.create()
    }

    init {
        fetchAllData()
    }

    fun fetchAllData() {
        loadBannerData()
        loadProducts()
    }

    //call api to fetch products from server
    private fun loadProducts() {
        if (GenericUtils.isNetworkConnectionAvailable()) {
            showProgressBar.value = true
            disposable = netWorkApi.getProductListData().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ data ->
                    Logger.log("product list fetch success ")
                    updateDatabase(data.categories)
                    _productList.value = data.categories
                    showProgressBar.value = false;
                },
                    { e ->
                        Logger.log("product list fetch failed  ---error ${e.message} ")
                        loadFromCachedData()
                        showProgressBar.value = false;
                    })
        } else {
            loadFromCachedData()
        }
    }

    //checks if valid data exists in database
    private fun loadFromCachedData() {
        viewModelScope.launch {
            _productList.value = dao.getAllData()
        }
    }


    //save product items into database
    private fun updateDatabase(categories: List<Categories>) {
        viewModelScope.launch {
            categories?.let {
                dao.deleteAll() // clear DB items
                dao.insert(categories) // insert new items
                Logger.log("insert ${categories.size}")
            }
        }
    }

    //call API to fetch the banner details
    private fun loadBannerData() {
        if (GenericUtils.isNetworkConnectionAvailable()) {
            disposable = netWorkApi.getBannerListData().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ data ->
                    Logger.log("banner fetch success ---${data.get(0).imageUrl} ")
                    _bannerDataList.value = data;

                }, { e -> Logger.log("banner fetch failed ---error ${e.message} ") })
        }
    }

    override fun onCleared() {
        super.onCleared()
        disposable?.dispose()
    }

    // Add item to cart list
    fun addToCart(categoryItem: CategoryItems?) {
        val cartList = _cartList.value
        if (cartList == null) _cartList.value = ArrayList() //init list view if empty
        categoryItem?.let {
            try {
                it.itemPrice = it.item_amount.toInt()
            } catch (e: Exception) {
                it.itemPrice = 0
                Logger.log(e.localizedMessage) //failed to parse string
            }
            _cartList.value!!.add(it)
        }

        cartTotal.value = _cartList?.value?.sumBy { it.itemPrice }
            .toString() //calculate to total price of cart items
    }
}
