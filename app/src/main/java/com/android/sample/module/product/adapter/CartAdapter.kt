package com.android.sample.module.product.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.android.sample.R
import com.android.sample.model.CategoryItems
/**
 * Created by Supriya on 15/04/20.
 */
class CartAdapter(val items: ArrayList<CategoryItems>, val context: Context) :
    RecyclerView.Adapter<ViewHolder>() {

    // Gets the number of animals in the list
    override fun getItemCount(): Int {
        return items.size
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.cart_item, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items.get(position)
        holder.tvTitle?.text = item.item_name
        holder.tvPrice?.text =
            String.format(context.getString(R.string.price), item.item_amount)
    }
}

class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    // Holds the TextView that will add each animal to
    val tvTitle = view.findViewById<TextView>(R.id.item_title)
    val tvPrice = view.findViewById<TextView>(R.id.item_price)
}