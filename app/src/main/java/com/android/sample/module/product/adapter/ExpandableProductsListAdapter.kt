package com.android.sample.module.product.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseExpandableListAdapter
import android.widget.ImageView
import android.widget.TextView
import com.android.sample.R
import com.android.sample.model.Categories
import com.android.sample.model.CategoryItems
/**
 * Created by Supriya on 15/04/20.
 */

class ExpandableProductsListAdapter(
    private val context: Context,
    var productsList: List<Categories>,
    var listener: IProductClickListener?
) : BaseExpandableListAdapter() {

    override fun getChild(groupPosition: Int, childPosititon: Int): Any {
        return productsList[groupPosition]?.category_items[childPosititon]
    }

    override fun getChildView(
        groupPosition: Int, childPosition: Int,
        isLastChild: Boolean, convertView: View?, parent: ViewGroup
    ): View {
        var convertView = convertView

        //inflate the view
        if (convertView == null) {
            val inflater = this.context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            convertView = inflater.inflate(R.layout.item_category_child, null)
        }

        //set the category items
        convertView?.let {
            val titleTextView = convertView.findViewById<TextView>(R.id.item_title)
            val priceTextView = convertView.findViewById<TextView>(R.id.item_price)
            val ivAddToCart = convertView.findViewById<ImageView>(R.id.iv_add_to_cart)

            val categoryItem: CategoryItems? =
                productsList[groupPosition].category_items[childPosition]
            titleTextView.text = categoryItem?.item_name
            priceTextView.text =
                String.format(context.getString(R.string.price), categoryItem?.item_amount)

            //listen to the item click
            ivAddToCart.setOnClickListener(View.OnClickListener {
                    listener?.onProdClick(categoryItem)
            })
        }
        return convertView!!
    }

    override fun getChildrenCount(groupPosition: Int): Int {
        return this.productsList.get(groupPosition).category_items.size
    }

    override fun getGroup(groupPosition: Int): Any {
        return this.productsList[groupPosition]
    }

    override fun getGroupCount(): Int {
        return this.productsList.size
    }

    override fun getGroupId(groupPosition: Int): Long {
        return groupPosition.toLong()
    }


    override fun getChildId(groupPosition: Int, childPosition: Int): Long {
        return childPosition.toLong()
    }

    /**
     * set the parent view of the list
     */
    override fun getGroupView(
        groupPosition: Int, isExpanded: Boolean,
        convertView: View?, parent: ViewGroup
    ): View {
        var convertView = convertView
        if (convertView == null) {
            val inflater = this.context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            convertView = inflater.inflate(R.layout.item_category_parent, null)
        }

        // set the parent title
        convertView?.let {
            val title = convertView.findViewById<TextView>(R.id.product_title)

            val categoryItem: Categories? =
                productsList[groupPosition]
            title.text = categoryItem?.category_name

            val imageView =
                convertView.findViewById<ImageView>(R.id.chevron_image)
            if (!isExpanded) {
                imageView.setImageDrawable(context.getDrawable(R.drawable.ic_chevron_right))
            } else {
                imageView.setImageDrawable(context.getDrawable(R.drawable.ic_expand_down))
            }
        }
        return convertView!!
    }

    override fun hasStableIds(): Boolean {
        return false
    }

    override fun isChildSelectable(groupPosition: Int, childPosition: Int): Boolean {
        return true
    }
}