package com.android.sample.module.product.ui

import android.os.Bundle
import android.view.*
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.sample.R
import com.android.sample.databinding.FragmentCartBinding
import com.android.sample.module.product.adapter.CartAdapter
import com.android.sample.module.product.viewmodel.ProductViewModel
/**
 * Created by Supriya on 15/04/20.
 */
class CartFragment : Fragment() {

    private var viewModel: ProductViewModel? = null
    private lateinit var fragmentCartBinding: FragmentCartBinding

    companion object {
        fun newInstance() = CartFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        fragmentCartBinding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_cart, container, false
        )
        setHasOptionsMenu(true)
        return fragmentCartBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        //initialise view model
        viewModel = activity?.let { ViewModelProvider(it).get(ProductViewModel::class.java) }
        fragmentCartBinding.lifecycleOwner = this
        fragmentCartBinding.productViewModel = viewModel
        initListView()
        viewModel?.cartList?.observe(viewLifecycleOwner, Observer { cartItems ->
            if (cartItems.isNotEmpty()) {
                fragmentCartBinding.cartListView.adapter = activity?.let { CartAdapter(cartItems, it) }
            } else {
                fragmentCartBinding.cartAnim.playAnimation()
            }
        })

    }

    private fun initListView() {

        fragmentCartBinding.cartListView.layoutManager =
            LinearLayoutManager(activity, RecyclerView.VERTICAL, false)

    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        val item = menu.findItem(R.id.action_cart)
        item?.isVisible = false
        super.onCreateOptionsMenu(menu, inflater)
    }

}
