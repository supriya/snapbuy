package com.android.sample.module.product.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.viewpager.widget.PagerAdapter
import com.android.sample.R
import com.android.sample.model.BannerListItem
import com.bumptech.glide.Glide
/**
 * Created by Supriya on 15/04/20.
 */
class BannerAdapter(private var context: Context, private var list: List<BannerListItem>?) : PagerAdapter() {

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }

    override fun getCount(): Int {
       return list?.size?:0;
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val inflater = LayoutInflater.from(context)
        val layout = inflater.inflate(R.layout.image_item, container, false)
        val imageview: ImageView = layout.findViewById(R.id.image)
        //initialise the glide image view to lod the image
        Glide.with(context).load(list?.let { it[position]
            .imageUrl }).placeholder(R.drawable.ic_no_image)
            .into(imageview)
        container.addView(layout)
        return layout
    }

    override fun destroyItem(container: ViewGroup, position: Int, view: Any) {
        container.removeView(view as View)
    }
}