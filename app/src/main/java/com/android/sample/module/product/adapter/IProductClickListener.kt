package com.android.sample.module.product.adapter

import com.android.sample.model.CategoryItems
/**
 * Created by Supriya on 15/04/20.
 */
interface IProductClickListener {
    fun onProdClick(categoryItem: CategoryItems?)
}