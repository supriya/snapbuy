package com.android.sample.module.splash

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import androidx.appcompat.app.AppCompatActivity
import com.airbnb.lottie.LottieAnimationView
import com.android.sample.R
import com.android.sample.module.product.ui.ProductListActivity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
/**
 * Created by Supriya on 15/04/20.
 */
class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        val lavSplash = findViewById<LottieAnimationView>(R.id.lav_splash);
        lavSplash.playAnimation();

        //launch the list activity
        GlobalScope.launch(context = Dispatchers.Main) {
            delay(3000)
            startActivity(Intent(this@SplashActivity, ProductListActivity::class.java))
            finish()
        }

    }

}
