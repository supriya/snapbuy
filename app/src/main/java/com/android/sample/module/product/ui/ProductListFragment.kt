package com.android.sample.module.product.ui

import android.content.Context
import android.os.Bundle
import android.view.*
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.android.sample.R
import com.android.sample.databinding.ProductListFragmentBinding
import com.android.sample.model.CategoryItems
import com.android.sample.module.IShopCommunicator
import com.android.sample.module.product.adapter.BannerAdapter
import com.android.sample.module.product.adapter.ExpandableProductsListAdapter
import com.android.sample.module.product.adapter.IProductClickListener
import com.android.sample.module.product.viewmodel.ProductViewModel
import com.android.sample.utils.GenericUtils
/**
 * Created by Supriya on 15/04/20.
 */
class ProductListFragment : Fragment(), IProductClickListener {

    private var communicator: IShopCommunicator? = null
    private lateinit var expandableListAdapter: ExpandableProductsListAdapter
    private var viewModel: ProductViewModel? = null
    private lateinit var productListFragmentBinding: ProductListFragmentBinding

    companion object {
        fun newInstance() =
            ProductListFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        setHasOptionsMenu(true)
        productListFragmentBinding = DataBindingUtil.inflate(
            inflater,
            R.layout.product_list_fragment, container, false
        )
        return productListFragmentBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        //initialise view model
        viewModel = activity?.let { ViewModelProvider(it).get(ProductViewModel::class.java) }
        productListFragmentBinding.productViewModel = viewModel
        productListFragmentBinding.lifecycleOwner = this

        //observe banner list
        viewModel?.bannerDataList?.observe(viewLifecycleOwner, Observer { list ->
            //method to check null
            list?.let {
                productListFragmentBinding.viewPager.adapter =
                    context?.let { BannerAdapter(it, list) }
            }
        })

        //observe product list
        viewModel?.productList?.observe(viewLifecycleOwner, Observer { productList ->
            //one more method to have a empty check
            if(productList.isNotEmpty()){
                productListFragmentBinding.noNetworkLayout.visibility = View.GONE
                expandableListAdapter = ExpandableProductsListAdapter(activity!!, productList,this)
                productListFragmentBinding.productsList.setAdapter(expandableListAdapter)
            } else {
                productListFragmentBinding.noNetworkLayout.visibility = View.VISIBLE
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        activity?.menuInflater?.inflate(R.menu.main_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id: Int = item.itemId
        if (id == R.id.action_cart) {
            communicator?.launchCartsFragment()
            return false
        }
        return super.onOptionsItemSelected(item)

    }

    override fun onProdClick(categoryItem: CategoryItems?) {
        viewModel?.addToCart(categoryItem)
        categoryItem?.item_name?.let { GenericUtils.showSnackBar(productListFragmentBinding.productlist, it) }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        communicator = context as IShopCommunicator?
    }
}
