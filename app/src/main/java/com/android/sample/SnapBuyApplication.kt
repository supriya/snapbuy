package com.android.sample

import android.app.Application
/**
 * Created by Supriya on 15/04/20.
 */
class SnapBuyApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        Companion.instance = this
    }

    companion object {
        lateinit var instance: SnapBuyApplication
    }
}