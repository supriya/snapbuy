package com.android.sample.network

import com.android.sample.BuildConfig
import com.android.sample.model.BannerListItem
import com.android.sample.model.ProductListItem
import io.reactivex.Observable
import retrofit2.Retrofit.Builder
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
/**
 * Created by Supriya on 15/04/20.
 */
interface NetWorkApi {

    @GET(BuildConfig.BANNER_API)
    fun getBannerListData():Observable<List<BannerListItem>>

    @GET(BuildConfig.PRODUCT_LIST_API)
    fun getProductListData():Observable<ProductListItem>

    companion object {
        fun create(): NetWorkApi {

            val retrofit = Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BuildConfig.BASE_URL)
                .build()

            return retrofit.create(NetWorkApi::class.java)
        }
    }

}