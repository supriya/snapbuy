package com.android.sample.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.android.sample.utils.AppConstants
import java.io.Serializable

/**
 * Created by Supriya on 15/04/20.
 */
@Entity(tableName = AppConstants.CATEGORY_TABLE_NAME)
data class Categories  (
     var category_items: List<CategoryItems>,
     var category_name: String,
     @PrimaryKey
     var category_id: String
):Serializable