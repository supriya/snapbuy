package com.android.sample.model

import java.io.Serializable
/**
 * Created by Supriya on 15/04/20.
 */

class CategoryItems(
    var item_id: String,
    var item_name: String,
    var item_amount: String,
    var itemPrice: Int
) : Serializable