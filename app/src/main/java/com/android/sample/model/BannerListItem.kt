package com.android.sample.model

import java.io.Serializable
/**
 * Created by Supriya on 15/04/20.
 */
data class BannerListItem (
    var name : String,
    var imageUrl : String
): Serializable
