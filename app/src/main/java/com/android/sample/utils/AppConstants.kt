package com.android.sample.utils

/**
 * Created by Supriya on 15/04/20.
 */
class AppConstants {
    companion object {
        val SHOP_DATA_BASE = "shop_data_base"
        const val CATEGORY_TABLE_NAME = "category_list_table"
    }
}