package com.android.sample.utils

import android.content.Context
import android.content.DialogInterface
import android.graphics.Color
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.view.View
import androidx.appcompat.app.AlertDialog
import com.android.sample.SnapBuyApplication
import com.android.sample.R
import com.google.android.material.snackbar.Snackbar

/**
 * Created by Supriya on 15/04/20.
 */
object GenericUtils {

    // added as an instance method to an Activity
    fun isNetworkConnectionAvailable(): Boolean {
        val cm =
            SnapBuyApplication.instance.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        val networks = cm.allNetworks
        var hasInternet = false
        if (networks.isNotEmpty()) {
            for (network in networks) {
                val nc = cm.getNetworkCapabilities(network)
                if (nc.hasCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET))
                    hasInternet = true
            }
        }
        return hasInternet
    }

    fun showAlertDialog(context: Context, message: String) {
        // build alert dialog
        val dialogBuilder = AlertDialog.Builder(context)
        // set message of alert dialog
        dialogBuilder.setMessage(message)
            // if the dialog is cancelable
            .setCancelable(false)
            // positive button text and action
            .setPositiveButton(
                context.getString(R.string.OK),
                DialogInterface.OnClickListener { dialog, _ ->
                    dialog.dismiss()
                })

        // create dialog box
        val alert = dialogBuilder.create()
        // set title for alert dialog box
        alert.setTitle(context.getString(R.string.error_title))
        // show alert dialog
        alert.show()
    }

    fun showSnackBar(view: View, item: String) {
        //Snackbar(view)
        val snackbar = Snackbar.make(
            view, "$item added to cart.",
            Snackbar.LENGTH_LONG
        ).setAction("ok", null)
        snackbar.setActionTextColor(Color.BLUE)
        snackbar.show()
    }
}