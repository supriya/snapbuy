package com.android.sample.utils

import android.util.Log
import com.android.sample.BuildConfig
/**
 * Created by Supriya on 15/04/20.
 */
object Logger {
    fun log(msg: String) {
        if (BuildConfig.IS_LOGS_ENABLED) {
            Log.d(BuildConfig.TAG, msg)
        }
    }
}