package com.android.sample.database

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.android.sample.model.Categories
import com.android.sample.model.CategoryItems
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.runBlocking
import org.junit.*
import org.junit.runner.RunWith
import java.io.IOException

/**
 * Created by Supriya on 15/04/20.
 */

@RunWith(AndroidJUnit4::class)
class ProductListDaoTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var productListDao: ProductListDao
    private lateinit var db: ProductListDataBase

    @Before
    fun createDb() {
        val context: Context = ApplicationProvider.getApplicationContext()
        // Using an in-memory database because the information stored here disappears when the
        // process is killed.
        db = Room.inMemoryDatabaseBuilder(context, ProductListDataBase::class.java)
            // Allowing main thread queries, just for testing.
            .allowMainThreadQueries()
            .build()
        productListDao = db.productListDao()
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        db.close()
    }


    @Test
    @Throws(Exception::class)
    fun getAllWords() = runBlocking {
        val CategoryItems = CategoryItems("1", "Product-1", "100", 100)
        val CategoryItems2 = CategoryItems("2", "Product-2", "100", 100)
        val CategoryItems3 = CategoryItems("3", "Product-3", "100", 100)

        var list = mutableListOf<CategoryItems>()
        list.add(CategoryItems)
        list.add(CategoryItems2)
        list.add(CategoryItems3)

        val prodList = mutableListOf<Categories>()
        val Category = Categories(list, "Categories-1", "1")
        val Category2 = Categories(list, "Categories-2", "2")
        val Category3 = Categories(list, "Categories-3", "3")
        prodList.add(Category)
        prodList.add(Category2)
        prodList.add(Category3)
        productListDao.insert(prodList)
        val allWords = productListDao.getAllData()
        assertEquals(allWords[0].category_name, Category.category_name)
        assertEquals(allWords[1].category_name, Category2.category_name)
    }

    @Test
    @Throws(Exception::class)
    fun deleteAll() = runBlocking {
        val CategoryItems = CategoryItems("1", "Product-1", "100", 100)
        val CategoryItems2 = CategoryItems("2", "Product-2", "100", 100)
        val CategoryItems3 = CategoryItems("3", "Product-3", "100", 100)

        var list = mutableListOf<CategoryItems>()
        list.add(CategoryItems)
        list.add(CategoryItems2)
        list.add(CategoryItems3)

        val prodList = mutableListOf<Categories>()
        val Category = Categories(list, "Categories-1", "1")
        val Category2 = Categories(list, "Categories-2", "2")
        val Category3 = Categories(list, "Categories-3", "3")
        prodList.add(Category)
        prodList.add(Category2)
        prodList.add(Category3)
        productListDao.insert(prodList)
        productListDao.deleteAll()
        val allWords = productListDao.getAllData()
        Assert.assertTrue(allWords.isEmpty())
    }

}